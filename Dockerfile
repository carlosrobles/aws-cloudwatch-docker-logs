from alpine:latest

MAINTAINER carlos.robles@gmail.com

# print current unix time in milliseconds indefinitely
CMD while true; do date +%s%3N && sleep 1; done
